from typing import Optional, Dict

from fastapi import HTTPException
from fastapi.security import OAuth2PasswordBearer
from passlib.context import CryptContext
from pydantic import BaseModel
from jose import jwt
from fastapi.security import HTTPBearer, HTTPAuthorizationCredentials

from src.auth.models import DBUser, get_user
from fastapi.requests import Request
from http import HTTPStatus


AUTH_USER_MODEL = 'AuthUser'


class BaseAuthentication:
    pass





