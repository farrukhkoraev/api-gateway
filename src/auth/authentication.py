from http import HTTPStatus
from typing import Optional

from fastapi import HTTPException
from fastapi.security import HTTPBearer, HTTPAuthorizationCredentials
from fastapi.requests import Request

from src.auth.common import decode_jwt


class JWTBearer(HTTPBearer):
    errors = {
        "credentials": "Authentication credentials not given",
        "schema": "Invalid authentication schema",
        "token": "Invalid token or expired"
    }

    def __init__(self, auto_error: bool = True):
        super().__init__(auto_error=auto_error)

    def fail(self, code: str):
        raise HTTPException(status_code=HTTPStatus.FORBIDDEN, detail=self.errors[code])

    def validated_token(self, token: str) -> Optional[dict]:
        try:
            claims = decode_jwt(token)
            return claims
        except Exception as e:
            return None

    async def __call__(self, request: Request):
        credentials: HTTPAuthorizationCredentials = await super().__call__(request)
        if not credentials:
            self.fail('credentials')
        if credentials.scheme != 'Bearer':
            self.fail('schema')
        validated_token = self.validated_token(credentials.credentials)
        if not validated_token:
            self.fail('token')

        return validated_token

