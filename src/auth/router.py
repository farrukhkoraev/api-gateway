from http import HTTPStatus

from fastapi import APIRouter, HTTPException
from pydantic import BaseModel

from .common import get_jwt_token, fake_users_db as db
from .models import get_user, DBUser

router = APIRouter()


class SignInBody(BaseModel):
    username: str
    password: str


@router.post(path='/signin/', tags=['auth'])
def sign_in(body: SignInBody):
    user: DBUser = get_user(db, body.username)
    if not user:
        raise HTTPException(
            status_code=HTTPStatus.BAD_REQUEST,
            detail='User with this credentials not found'
        )
    if not user.verify_password(body.password):
        raise HTTPException(
            status_code=HTTPStatus.BAD_REQUEST,
            detail='User with this credentials not found'
        )
    data = {
        'access_token': get_jwt_token(user.username)
    }

    return data

