from typing import Optional

from jose import jwt
from passlib.context import CryptContext
from pydantic import BaseModel

SECRET_KEY = "c26ccee7d2b4de2a617a35cf497a875207d9ac5072b6dfa925e2ed67fd70a78b"
ALGORITHM = "HS256"
ACCESS_TOKEN_EXPIRE_MINUTES = 30

pwd_contex = CryptContext(schemes=["bcrypt"], deprecated="auto")


class BaseUser(BaseModel):
    username: str
    full_name: str
    email: Optional[str] = None
    is_active: bool = False


def get_jwt_token(user_id: str) -> str:
    payload = {
        'user_id': user_id,
        'expires': ACCESS_TOKEN_EXPIRE_MINUTES
    }
    token = jwt.encode(payload, key=SECRET_KEY, algorithm=ALGORITHM)

    return token


def decode_jwt(token: str) -> dict:
    return jwt.decode(token, key=SECRET_KEY, algorithms=[ALGORITHM])


fake_users_db = {
    "johndoe": {
        "username": "johndoe",
        "full_name": "John Doe",
        "email": "johndoe@example.com",
        "password": "$2b$12$lDBmWLuQkdMVe8E7ncGoneDSqG.oQmZJf8Gmlj3tzZIFljuGZrSsW",
        "disabled": False,
    }
}
