from typing import Optional

from src.auth.common import BaseUser, pwd_contex


class DBUser(BaseUser):
    password: str

    def verify_password(self, raw_password):
        return pwd_contex.verify(raw_password, self.password)

    def hash_password(self, raw_password):
        self.password = pwd_contex.hash(raw_password)

#
# def verify_password(plain_password: str, hashed_password: str) -> bool:
#     return pwd_contex.verify(plain_password, hashed_password)
#
#
# def get_hashed_password(plain_password: str) -> str:
#     return pwd_contex.hash(plain_password)


def get_user(db, username: str) -> Optional[DBUser]:
    if username in db:
        return DBUser(**db[username])
    return None

