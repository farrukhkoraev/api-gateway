from typing import Annotated

from fastapi import FastAPI, Depends
from fastapi.security import OAuth2PasswordBearer
from fastapi.routing import APIRouter
from src.auth.authentication import JWTBearer
from src.auth.router import router as auth_router

app = FastAPI()

oauth2_scheme = OAuth2PasswordBearer(tokenUrl="token")

app.include_router(auth_router)


@app.get("/items/")
async def read_items(current_user: Annotated[str, Depends(JWTBearer())]):
    return {"token": current_user}

